# Generated by Django 4.1.7 on 2023-03-10 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PWA', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('birthday', models.DateField(auto_created=True, blank=True, null=True)),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('text', models.TextField(blank=True, max_length=255, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='uploads/')),
                ('description', models.TextField(blank=True, max_length=255, null=True)),
                ('phone', models.IntegerField(blank=True, max_length=10, null=True)),
                ('city', models.CharField(blank=True, max_length=10, null=True)),
                ('age', models.IntegerField(blank=True, max_length=4, null=True)),
                ('degree', models.CharField(blank=True, max_length=50, null=True)),
                ('email', models.EmailField(max_length=255)),
            ],
            options={
                'verbose_name': 'About',
                'verbose_name_plural': 'Abouts',
            },
        ),
        migrations.DeleteModel(
            name='Project',
        ),
    ]
