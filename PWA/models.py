from django.db import models
from django_extensions.db.models import TimeStampedModel

from utils import AddressAbstract


# Create your models here.


class About(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    text = models.TextField(max_length=255, null=True, blank=True)
    image = models.ImageField('uploads/', null=True, blank=True)
    description = models.TextField(max_length=255, null=True, blank=True)
    birthday = models.DateField(auto_created=True, null=True, blank=True)
    phone = models.IntegerField(null=True, blank=True)
    city = models.CharField(max_length=10, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    degree = models.CharField(max_length=50, null=True, blank=True)
    email = models.EmailField(max_length=255)
    current_project = models.CharField(max_length=50, null=True, blank=True)
    current_project_link = models.URLField(max_length=255, default=False)

    class Meta:
        verbose_name = 'About'
        verbose_name_plural = 'Abouts'


class Education(models.Model):
    qualification = models.CharField(max_length=50, null=True, blank=True)
    start_batch = models.IntegerField(null=True, blank=True)
    end_batch = models.IntegerField(null=True, blank=True)
    college = models.TextField(max_length=255, null=True, blank=True)
    address = models.TextField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = 'Education'
        verbose_name_plural = 'Educations'

    def __str__(self):
        return self.qualification


class Skill(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    percentage = models.FloatField()

    class Meta:
        verbose_name = 'Skill'
        verbose_name_plural = 'Skills'

    def __str__(self):
        return self.name


class Service(models.Model):
    service_name = models.CharField(max_length=50, null=True, blank=True)
    service_type = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = "Service"
        verbose_name_plural = "Services"

    def __str__(self):
        return self.service_name


class Project(models.Model):
    image = models.ImageField('uploads/', null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True,blank=True)

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    def __str__(self):
        return self.title


class Contact(AddressAbstract):
    name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255)
    phone = models.IntegerField(null=True, blank=True)
    subject = models.TextField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return self.name








