from django.http import JsonResponse
from django.shortcuts import render, redirect
from PWA import models
from PWA.form import ContactForm
# Create your views here.


def about(request):
    about = models.About.objects.all()
    education = models.Education.objects.all()
    skill = models.Skill.objects.all()
    service = models.Service.objects.all()
    project = models.Project.objects.all()
    context = {'about': about, 'education': education, 'skill': skill, 'service': service, 'project': project}
    return render(request, 'project.html', context)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('about')
        else:
            return JsonResponse({"error": "something went wrong"})
    else:
        form = ContactForm()
    return render(request, 'project.html', {'form': form})
