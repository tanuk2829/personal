from django.contrib import admin
from PWA import models
# Register your models here.

admin.site.register(models.About)
admin.site.register(models.Education)
admin.site.register(models.Skill)
admin.site.register(models.Service)
admin.site.register(models.Project)
admin.site.register(models.Contact)