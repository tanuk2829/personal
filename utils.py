from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _


class AddressAbstract(models.Model):
    street = models.TextField(_('Colony / Street / Locality'), null=True, blank=True)
    city = models.CharField(_('City'), max_length=255, null=True, blank=True)
    state = models.CharField(_('State'), max_length=255, null=True, blank=True)

    class Meta:
        abstract = True